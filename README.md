# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

This application will pull in data from various platforms and agregate them into a central database.
Relationships will be created from these data sources to be "slinged" to other platforms for their use.
For example: data will be pulled from a CRM system and posted over to a Issue tracking system so contact information 
is kept up to date and in sync.

### How do I get set up? ###

* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact